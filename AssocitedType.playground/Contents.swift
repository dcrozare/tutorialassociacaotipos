import UIKit

//: Criação do protocolo com associação de tipo
//:
//: [Link para google](https://www.globo.com)
enum Type {

}

protocol DataType {
    associatedtype Data
    var arrayData: [Data] { get set }
    
    mutating func add(value: Data)
}

extension DataType {
    mutating func add(value: Data) {
        arrayData.append(value)
    }
}
//: Usando a associação de tipos em diferentes classes

struct Teste: DataType {
    var arrayData: [String] = []
}

struct Teste1: DataType {
    var arrayData: [Int] = []
}

struct Teste2: DataType {
    var arrayData: [Double] = []
}

var teste1 = Teste1()
teste1.add(value: 1)
teste1.add(value: 3)
teste1.add(value: 3)
teste1.add(value: 4)

print(teste1.arrayData)

var teste = Teste()
teste.add(value: "Diego")
teste.add(value: "Gustavo")
teste.add(value: "Arthur")
teste.add(value: "Josimeire")

print(teste.arrayData)

var teste2 = Teste2()
teste2.add(value: 1)
teste2.add(value: 3)
teste2.add(value: 3)
teste2.add(value: 4)

print(teste2.arrayData)



protocol switchOn {
    func on()
}

protocol switchOff {
    func off()
}

class testeSwitc: switchOn, switchOff {
    
    var status: Bool = false
    func on() {
        status = true
    }
    
    func off() {
        status = false
    }
    
    var description: String {
        return "status \(String(describing: status))"
    }
}

    let testando = testeSwitc()
    testando.on()
    testando.description

protocol Executable {
    func execute()
}

class ExecutableSwitch: Executable {
    
    private let aSwitch: switchOn
    
    init(_ aSwitch: switchOn) {
        self.aSwitch = aSwitch
    }
    
    func execute() {
        self.aSwitch.on()
    }
}

class ExecutableSwitchOff: Executable {
    
    private let aSwitch: switchOff
    
    init(_ aSwitch: switchOff) {
        self.aSwitch = aSwitch
    }
    
    func execute() {
        self.aSwitch.off()
    }
}

let testandoSwitch = testeSwitc()

testandoSwitch.description

let testeOn = ExecutableSwitch(testandoSwitch)
testeOn.execute()
testandoSwitch.description
let testeOff = ExecutableSwitchOff(testandoSwitch)
testeOff.execute()
testandoSwitch.description

let image = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))

image.backgroundColor = .white
